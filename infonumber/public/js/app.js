/* 
 * app.js
 * contactNumber
 * @Author Brian
 */

function decrypt(x, y, z){
    document.getElementById(y).value = "";
    var bs = document.getElementById(x).value;
    var bs64 = atob(bs);
    document.getElementById(x).value = bs64;
    var n = document.getElementById(z).value.toString().length;
    decryptData.Decrypt(x, y, z);
    decryptData.Decrypt(y, y, z);
    for(var i = 0; i < n/3; i++){
        decryptData.Decrypt(y, y, z);   
    }
    var dc = document.getElementById(y).value;
    bs64 = atob(dc);
    document.getElementById(y).value = bs64;
    document.getElementById(x).value = bs;
}

var decryptData = {
    Decrypt: function(x, y, z) {
        var estr = document.getElementById(x).value;
        var cde = document.getElementById(z).value;
        document.getElementById(y).value = this.decrypted(estr, cde);
    },
    decrypted: function(hx, code) {
        var bx = hexToBytes(hx);
        var ac2 = [];
        var ba = bx.subarray();
        for(var a in ba){
            ac2.push(ba[a]);
        }
        var jd2 = JSON.stringify(ac2);
        var result = [];
        var str = '';
        var ArrD = JSON.parse(jd2);
        var codeLen = code.length ;
        for(var i = 0  ; i < ArrD.length ; i++) {
            var Offset = i%codeLen ;
            var AsciiN = (ArrD[i]-code.charCodeAt(Offset));
            result.push(AsciiN) ;
        }
        for(var i = 0 ; i < result.length ; i++) {
            var ch = String.fromCharCode(result[i]); 
            str += ch ;
        }
        return str ;
    }   
};

function hexToBytes(str) {
    if (!str) {
        return new Uint8Array();
    }
    var a = [];
    for (var i = 0, len = str.length; i < len; i += 2) {
        a.push(parseInt(str.substr(i, 2), 16));
    }
    return new Uint8Array(a);
}

function reset(x, y, z, h){
    var ax = xor(x, y);
    ax += xor("1234567890", "0987654321");
    ax += xor("001", "011");
    var jdata = JSON.stringify(ax);
    document.getElementById(y).value = "";
    document.getElementById(x).value = "";
    document.getElementById(y).value = jdata;
    document.getElementById(z).value = "";
    document.getElementById(h).innerHTML = "Help";
}

function xor(v, w) {
    var res = [];
    var x;
    if (v.length > w.length) {
        for (var i = 0; i < w.length; i++) {
            res.push(v[i] ^ w[i]);
        }
    } 
    else {
        for (var i = 0; i < v.length; i++) {
           x = (v[i] ^ w[i]);
           res.push(x);
        }
    }
    return res;
}



function help(x, y, z, h){
    var key = "Njg2ODY3NzFhNGFjYTQ5M2M2YTY5NDZkOTM2Mzk0NmNkMWFiY2U5ODllYTM2OTk4NmI2MzY3NmNhN2E5YTNjN2M5YTM5MTk5OTU5NTZhNzFhN2RhY2U5NTliYWI2OTZmNjg5MjY3NmNhNGFjYTNjNWM2YTk5MTcwOTM2Mzk0NmZkMWFlY2U5NTllYTM2OTcwNmI2NTZhNmVhNGQ2YTQ5M2M2YTk5NDk4OTU5NTZhNzFhN2Q3Y2U5YTliYWI2OTcwNjg2ODZhNmJhNGFjYTNjN2M2YTY5NDZkOTM2Mzk0NzBkMWFiY2U5YjllYTM2NzY3NmI2MzY3NmRhNGQ2YTNjNGM5YTM5MTljOTU5NTZhNmVhN2RhY2U5NTliYWI2OTk4Njg5MjY3NmVhNGQ2YTNjN2M2YTY5NDZkOTM2Mzk0NzFkMWFiY2U5YjllYTY2OTZmNmI2NTZhNmVhNGQ2YTNjOGM2YTk5NDZlOTU5NTk0NmRhN2Q3Y2VjNDliYTg2OTliNjg2ODY3NzFhNGQ2YTNjNGM2YTY5MTlhOTM2Mzk0NmZkMWFiY2U5ODllYTM2OTk4NmI2MzY3NmNhN2E5YTNjN2M5YTM5MTk5OTU5NTZhOWJhN2RhY2U5NTliYTk2NjZlNjg5MjY3NmNhNGFjYTNjOGM2YTk5MTcwOTM2Mzk0NzBkMWFlY2U5NTllYTM2OTlhNmI2NTZhNmVhNGQ2YTNjN2M2YTk5NDk4OTU5NTZhNmVhN2Q3Y2VjNDliYWI2OTZmNjg2ODZhNmJhNGFjYTNjN2M2YTY5NDZkOTM2Mzk0NzFkMWFiY2U5ODllYTY2NjliNmI2MzY3NmRhNGQ2YTNjNGM5YTM5MTk5OTU5NTZhOWJhN2RhY2U5ODliYTg2OTliNjg5MjY3NmVhNGQ2YTNjN2M2YTY5NDZkOTM2Mzk0NzFkMWFiY2U5YjllYTY2NjljNmI2NTZhNmVhN2E5YTNjNWM2YTk5NDZlOTU5NTZhOWFhN2Q3Y2VjNDliYWI2NzY3Njg2ODY3NzFhNGFjYTQ5M2M2YTY5NDZkOTM2Mzk0NmRkMWFiY2U5ODllYTM2OTk4NmI2MzY3NmNhN2E5YTQ5M2M5YTM5MTk5OTU5NTZhNzFhN2RhY2U5NTliYWI2OTk4Njg5MjY3NmNhNGFjYTNjNmM2YTY5NDZkOTM2Mzk0NmRkMWFlY2U5NTllYTM2OTcwNmI2NTZhNmVhNGQ2YTQ5M2M2YTk5NDk4OTU5NTZhNmVhN2Q3Y2U5YTliYWI2OTk4Njg2ODZhNmJhNGFjYTNjN2M2YTY5NDZkOTM2Mzk0NzBkMWFiY2U5ODllYTY2NjliNmI2MzY3NmNhN2E5YTNjOGM5YTM5MTljOTU5NTZhNmVhN2RhY2U5NTliYWI2OTcwNjg5MjY3NmVhNGQ2YTNjN2M2YTk5MTcwOTM2Mzk0NmVkMWFiY2U5YjllYTY2OTZmNmI2NTZhNmVhNGQ2YTQ5MmM2YTk5NDZlOTU5NTk0NmRhN2Q3Y2VjNDliYTk2NjZlNjg2ODY3NzFhNGQ2YTNjNGM2YTY5MTlhOTM2Mzk0NmZkMWFiY2U5ODllYTM2OTk4NmI2MzY3NmNhN2E5YTQ5M2M5YTM5MTk5OTU5NTZhNzFhN2RhY2U5ODliYTg2OTk5Njg5MjY3NmNhNGFjYTNjNmM2YTY5NDZkOTM2Mzk0NmVkMWFlY2U5NTllYTM2OTk5NmI2NjY3NmFhNGQ2YTZjM2M2YTk5NDk4OTU5NTZhNzFhN2Q3Y2VjNDliYTg2OTliNjg2ODZhNmJhNGFjYTNjN2M2YTY5NDZkOTM2Mzk0NzFkMWFiY2U5ODllYTY2NjliNmI2MzY3NmRhNGQ2YTNjNGM5YTM5MTljOTU5NTZhNmVhN2RhY2U5NTliYWI2NzY3Njg5MjY3NmVhNGQ2YTNjN2M2YTY5NDZkOTM2NDZhNmJkMWFiY2U5YjllYTY2OTZmNmI2NTZhNmVhNGQ2YTNjOGM2YTk5NDZlOTU5NTZhOWFhN2Q3Y2U5YTliYWI2OTk4Njg2ODY3NzFhNGFjYTQ5M2M2YTY5NDZkOTM2Mzk0NmNkMWFiY2U5ODllYTM2OTk4NmI2MzY3NmNhNGQ2YTZjNGM5YTM5MTk5OTU5NTZhOWJhN2RhY2U5NTliYTg2OTlhNjg5MjY3NmNhNGFjYTNjNmM2YTY5NDZkOTM2Mzk0NmZkMWFlY2U5NTllYTM2OTcwNmI2NTZhNmVhNGQ2YTQ5NGM2YTk5NDk4OTU5NTZhNmVhN2Q3Y2U5YTliYWI2OTk4Njg2ODZhNmJhNGFjYTNjNWM2YTY5MTlhOTM2Mzk0NmVkMWFiY2U5YjllYTM2NzY3NmI2MzY3NmNhN2E5YTNjOGM5YTM5MTljOTU5NTZhNmVhN2RhY2U5NTliYWI2OTZmNjg5MjY3NmZhNGFjYTNjM2M2YTY5NDZkOTM2NDZhNmJkMWFiY2U5YjllYTY2OTZmNmI2NTZhNmVhNGQ2YTQ5NGM2YTk5NDZlOTU5NTk0NmRhN2Q3Y2VjNDliYWI2NjlkNjg2ODY3NzFhNGQ2YTNjNGM2YTY5MTlhOTM2Mzk0NmVkMWFiY2U5ODllYTY2NjliNmI2MzY3NmNhN2E5YTQ5MmM5YTM5MTk5OTU5NTZhNzFhN2RhY2U5ODliYTg2OTk5Njg5MjY3NmNhNGFjYTNjNWM2YTk5MTcwOTM2NDZhNmJkMWFlY2U5NTllYTM2OTlhNmI2NTZhNmVhNGQ2YTNjNmM2YTk5NDk4OTU5NTZhNmVhN2Q3Y2U5YTliYWI2OTk5Njg2ODZhNmJhNGFjYTNjOGM2YTY5MTlhOTM2Mzk0NmRkMWFiY2U5YjllYTM2NzY3NmI2MzY3NmNhN2E5YTQ5MmM5YTM5MTk5OTU5NTZhOWJhN2RhY2U5ODliYWI2NjlkNjg5MjY3NmVhNGQ2YTNjN2M2YTY5NDZkOTM2Njk0NmRkMWFiY2U5YjllYTY2NjljNmI2NTZhNmVhN2E5YTNjNmM2YTk5NDZlOTU5NTk0NmRhN2Q3Y2VjNDliYWI2NjlkNjg2ODY3NzFhNGFjYTQ5M2M2YTY5NDZkOTM2Mzk0NmNkMWFiY2U5ODllYTM2OTk4NmI2MzY3NmNhNGQ2YTZjNGM5YTM5MTk5OTU5NTZhNzFhN2RhY2U5ODliYTg2OTk5Njg5MjY3NmNhNGFjYTNjOGM2YTk5MTcwOTM2Mzk0NmVkMWFlY2U5NTllYTM2OTcwNmI2NTZhNmVhNGQ2YTQ5NGM2YTk5NDk4OTU5NTZhNzFhN2Q3Y2U5YTliYWI2OTcwNjg2ODZhNmJhNGFjYTNjN2M2YTY5NDZkOTM2Mzk0NzFkMWFiY2U5ODllYTY2NjliNmI2MzY3NmNhN2E5YTQ5MmM5YTM5MTljOTU5NTZhNmVhN2RhY2U5NTliYWI2OTZmNjg5MjY3NmZhNGFjYTNjM2M2YTY5NDZkOTM2NDZhNmNkMWFiY2U5YjllYTY2OTZmNmI2NTZhNmVhNGQ2YTQ5MmM2YTk5NDZlOTU5NTZhOWFhN2Q3Y2VjNDliYWI2NzY3Njg2ODY3NzFhNGQ2YTNjNGM2YTY5MTlhOTM2Mzk0NmVkMWFiY2U5ODllYTY2NjliNmI2MzY3NmNhN2E5YTNjNWM5YTM5MTk5OTU5NTZhNzFhN2RhY2U5ODliYTg2OTk5Njg5MjY3NmNhNGFjYTNjOGM2YTk5MTcwOTM2NDZhNmM=";
    var keyB = document.getElementById(z).value;
    if (keyB !== ""){
        document.getElementById(x).value = key;
        decrypt(x, y, z);
    }
    else{
        document.getElementById(z).style = "color: red";
        document.getElementById(h).innerHTML = "OK";
        document.getElementById(h).style = "color: blue";
    }
    
}